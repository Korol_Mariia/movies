// Тема: Знакомство с JavaScript. Переменные. Типы данных

// Задача 1) 
// Напишите программу, которая спрашивает у пользователя сколько ему лет (команда prompt()) и выводит его возраст на экран (команда alert()).

const userAge = prompt ('How old are you?');
alert (userAge);

// Задача 2) 
// Напишите программу, которая спрашивает у пользователя как его зовут, в какой стране и в каком городе он живет (команда prompt()) и выведите результат в консоль в следующем виде (команда console.log()). 

const userName = prompt ('What is your name?');
const userCity = prompt ('What city are you from?');
const userCountry = prompt ('Where are you from?');

console.log (`Hello, your name is ${userName}. You live in ${userCity}, ${userCountry}.`);

// Задача 3) 
// Напишите программу, которая выводит в консоль (команда console.log()) тип 3х разных переменных.
// Создайте переменную number и присвойте ей значение 156798;
// Создайте переменную string и присвойте ей значение IT School Hillel;
// Создайте переменную boolean и присвойте ей значение false;

const number = 156798;
const string = 'IT School "Hillel"';
const boolean = false;

console.log (`Value: ${number}; type: ${typeof number}`);
console.log (`Value: ${string}; type: ${typeof string}`);
console.log (`Value: ${boolean}; type: ${typeof boolean}`);

